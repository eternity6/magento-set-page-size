<?php
/**
 * Programmerrkt_PageSizeSetter extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE_PSS.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 *
 * @category       Programmerrkt
 * @package        Programmerrkt_PageSizeSetter
 * @copyright      Copyright (c) 2015   
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */

/**
 * Observer Class of extension Programmerrkt_PageSizeSetter
 *
 * @category    Programmerrkt
 * @package     Programmerrkt_PageSizeSetter
 * @author      Rajeev K Tomy <programmer.rkt@gmail.com>
 */
class Programmerrkt_PageSizeSetter_Model_Observer 
{
  
    /**
	 *
	 * Holds special category Id
	 *
	 * @var int
     */
	public $_categoryId = array();
	
	protected $_categoryId_desktop = array(
			'579', // Buste Shopper
			'592', // Shopper Carta
			'581',	// Sacchetti Buste Regalo
			'582', // Nastri Decorativi e Fiocchi
			'583',	// Accessori Per Confezionare
			'586', // Allestimento e Vetrofanie
			'591', // Monouso Ristorazione
			'819', // Speciale Natale 2018
	);
	
	protected $_categoryId_mobile = array(
			'579', // Buste Shopper
			'592', // Shopper Carta
			'581',	// Sacchetti Buste Regalo
			'582', // Nastri Decorativi e Fiocchi
			'583',	// Accessori Per Confezionare
			'586', // Allestimento e Vetrofanie
			'591', // Monouso Ristorazione
			'603', // Buste Regalo
			'599', //Carta Regalo a Fogli
			'595', //Shopper Juta e Cotone
			'598', // Shopper Portabottiglie 
			'680', //Nastri in Tessuto
			'685', //Nastri Tessuto Fantasia
			'610', //Fiocchi e Coccarde
			'612', //Chiudipacco e Pick
			'613', //Etichette Adesive e Stickers
			'678', //Bobine, Fogli e Cellophane
			'585', //Scatole e Cesti per Confezioni
			'744', //Scatole per Confezioni Regalo
			'618', //Cesti per Confezioni Regalo
			'743', //Scatole Portabottiglie
			'623', //Vetrofanie 
			'624', //Decorazioni e Pendenti per Vetrine
			'655', //Finger Food
			'651', //Piatti e Vassoi 
			'654', //Tovaglie e Tovaglioli 
			'666', //Tovaglioli 
			'805', //Accessori per la Tavola
			'806', //Accessori per la Tavola e Glacette
			'634', //Cancelleria
			'819', //Speciale natale 2018
			
	);

	/**
	 *
	 * Holds page Size
	 *
	 * @var int
	 */
	protected $_pageSize = array(
			'default' => '24',
			'mobile'	=>	'12',
	);

	/**
	 *
	 * Use to set Page Size
	 *
	 * @param    Varien_Event_Observer $observer
	 * @return   Programmerrkt_PageSizeSetter_Model_Observer
	 */
	public function setPageSizeForCategory(Varien_Event_Observer $observer)
	{
		
		
		/* controllo su che template mi trovo */
		$current_template = Mage::getSingleton('core/design_package')->getTheme('frontend');
		//Mage::log('Template code: '.$current_template, null, 'Eternity6_MagentoSetPageSize.log');
		
		switch($current_template){
			case 'mobile':
					$this->_categoryId = $this->_categoryId_mobile;
					$limit = $this->_pageSize['mobile'];
				break;
			case 'default':
					$this->_categoryId = $this->_categoryId_desktop;
					$limit = $this->_pageSize['default'];
				break;
		}
		
		
		foreach ($this->_categoryId AS $categoryId) {
			$currentCategory = Mage::registry('current_category');
			
			//check whether current page is a category page
			if ($currentCategory instanceof Mage_Catalog_Model_Category) {
				//ensure the the cateogory page is our specific category
				if ($currentCategory->getId() == $categoryId) {
			
					//check whether toolbar block exist or not
					$toolbar =  $observer->getLayout()->getBlock('product_list_toolbar');
					if ($toolbar) {
						//sets page size to corresponding mode
						$listMode = $toolbar->getCurrentMode();
						$toolbar = $toolbar->addPagerLimit($listMode , $limit);
					}
				}
			}
			
			
		}
		
		return $this;
		
		
	}
}
